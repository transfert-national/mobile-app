package com.example.transfertnational

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.transfertnational.network.NetworkingService

class App : Application() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        NetworkingService.init(this)
    }
}