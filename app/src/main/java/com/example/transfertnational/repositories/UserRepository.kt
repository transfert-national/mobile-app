package com.example.transfertnational.repositories

import com.example.transfertnational.network.params.LoginInput
import com.example.transfertnational.network.responses.LoginResponse
import com.example.transfertnational.network.NetworkingService
import com.example.transfertnational.network.responses.AccessToken
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field


class UserRepository{
    suspend fun login(loginInput: LoginInput): Response<LoginResponse> {
        return NetworkingService.apiRest.login(loginInput)
    }
    suspend fun getAccessToken(
        client_id:String,
        grant_type:String,
        client_secret:String,
        scope:String,
        username:String,
        password:String
    ):Response<AccessToken>{
        return NetworkingService.apiRestLogin.getAccessToken(client_id,grant_type,client_secret, scope, username, password)
    }
    fun logout(
        @Field("client_id") client_id:String,
        @Field("refresh_token") refresh_token:String,
        @Field("client_secret") client_secret:String
    ):Call<ResponseBody>{
        return NetworkingService.apiRestLogin.logout(client_id, refresh_token, client_secret)
    }
}