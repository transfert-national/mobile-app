package com.example.transfertnational.repositories

import com.example.transfertnational.data.enum.Etat
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.network.NetworkingService
import com.example.transfertnational.utils.Constants
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query


class TransfertRepository {
    suspend fun getTransfertsByUser(idUser: String?): Response<List<Transfert>> {
        return NetworkingService.apiRest.getTransfertsByUser(idUser)
    }

    suspend fun makeTransferFromCompteDebit(transfert: Transfert?): Response<Transfert> {
        return NetworkingService.apiRest.makeTransferFromCompteDebit(transfert)
    }

    suspend fun getTransfertByRef(ref: String?): Response<Transfert> {
        return NetworkingService.apiRest.getTransfertByRef(ref)
    }

    suspend fun getTransfertByEtatAndUser(
        etat: Etat?, idUser: String?
    ): Response<List<Transfert>> {
        return NetworkingService.apiRest.getTransfertByEtatAndUser(etat, idUser)
    }

    suspend fun changeTransferEtat(
        ref: String?, motif: String?
    ): Response<Transfert> {
        return NetworkingService.apiRest.changeTransferEtat(ref, motif)
    }
}
