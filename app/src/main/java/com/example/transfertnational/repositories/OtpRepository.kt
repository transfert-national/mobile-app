package com.example.transfertnational.repositories

import com.example.transfertnational.data.model.Otp
import com.example.transfertnational.network.NetworkingService
import retrofit2.Call
import retrofit2.http.Body


class OtpRepository{
    fun generateOtp(): Call<Void>{
        return NetworkingService.apiRest.generateClientOtp()
    }
    suspend fun verifyOtp( otp: Otp?): Boolean{
        return NetworkingService.apiRest.verifyOtp(otp)!!
    }
}
