package com.example.transfertnational.repositories

import com.example.transfertnational.data.model.Wallet
import com.example.transfertnational.network.NetworkingService
import retrofit2.Response
import retrofit2.http.Path
import retrofit2.http.Query

class WalletRepository{
    suspend fun getWalletByClient( registrationNumber: String?): Response<Wallet>{
        return NetworkingService.apiRest.getWalletByClient(registrationNumber)
    }
    suspend fun depositByClientId( registrationNumber: String?, amount: Double
    ): Response<Wallet>{
        return NetworkingService.apiRest.depositByClientId(registrationNumber, amount)
    }

}