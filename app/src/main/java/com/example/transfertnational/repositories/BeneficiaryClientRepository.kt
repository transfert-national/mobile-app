package com.example.transfertnational.repositories

import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.network.NetworkingService
import retrofit2.Response

class BeneficiaryClientRepository{
    suspend fun getBeneficiaryClientsByIdClient(idClient : String?): Response<List<BeneficiaryClient>> {
        return NetworkingService.apiRest.getBeneficiaryClientByClient(idClient)
    }
    suspend fun getBeneficiaryClient(id : String?): Response<BeneficiaryClient> {
        return NetworkingService.apiRest.getBeneficiaryClient(id)
    }

    fun searchBeneficiaryClient(list:List<BeneficiaryClient>,name: String): List<BeneficiaryClient> {
        return list.filter { client->
            client.toString().contains(name)
                         }

    }
    suspend fun addBeneficiaryClient(beneficiaryClient : BeneficiaryClient ):Response<BeneficiaryClient>{
        return NetworkingService.apiRest.addBeneficiaryClient(beneficiaryClient)
    }

}