package com.example.transfertnational.repositories

import com.example.transfertnational.data.model.Account
import com.example.transfertnational.network.NetworkingService
import com.example.transfertnational.network.params.WithdrawInput
import retrofit2.Response
import retrofit2.http.Body

class AccountRepository {
    suspend fun getAccountByClientId( clientId: String?): Response<Account>{
        return NetworkingService.apiRest.getAccountByClientId(clientId)
    }
    suspend fun withdrawByRegistrationNumber( withdrawDto: WithdrawInput): Response<Account>{
        return NetworkingService.apiRest.withdrawByRegistrationNumber(withdrawDto)
    }
}