package com.example.transfertnational.repositories

import com.example.transfertnational.data.model.Client
import com.example.transfertnational.network.NetworkingService
import retrofit2.Call
import retrofit2.Response

class ClientRepository {
    suspend fun getClientByRegistrationNumber(registrationNumber: String?): Response<Client> {
        return NetworkingService.apiRest.getClientByRegistrationNumber(registrationNumber)
    }

}