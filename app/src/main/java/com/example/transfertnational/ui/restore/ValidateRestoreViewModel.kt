package com.example.transfertnational.ui.restore

import androidx.lifecycle.*
import com.example.transfertnational.data.model.Client
import com.example.transfertnational.data.model.Otp
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.repositories.*
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class ValidateRestoreViewModel : ViewModel() {


    private val clientRepository = ClientRepository()
    private val walletRepository = WalletRepository()
    private val transfertRepository = TransfertRepository()
    private val beneficiaryClientRepository = BeneficiaryClientRepository()
    private val otpRepository = OtpRepository()


    val transfert = MutableLiveData<Transfert>()
    val otp = MutableLiveData<Otp>(Otp())
    var verify = MutableLiveData<Boolean>()
    var isenabled = MutableLiveData<Boolean>(false)
    //val client = MutableLiveData<Client>()
    val clientCall = MutableLiveData<Response<Client>>()
    val name = MutableLiveData<String>()
    val motif = MutableLiveData<String>()
    val nomBeneficiary = MutableLiveData<String>()
    val restoredTransfert= MutableLiveData<Response<Transfert>>()
    val valid = MediatorLiveData<Boolean>().apply{
        addSource(motif){
            value = isNameValid(it)
        }
    }


    fun getClient(registrationNumber: String?){
        viewModelScope.launch {
            val response =
                clientRepository.getClientByRegistrationNumber(registrationNumber)
            clientCall.value = response
        }

    }
    fun generateOtp(): Call<Void> {
        return otpRepository.generateOtp()
    }
    fun verifyOtp(otp:Otp?){
        viewModelScope.launch {
            val response = otpRepository.verifyOtp(otp)
            verify.value = response
        }
    }
    fun restoreTransfert(ref: String?, motif: String?){
        viewModelScope.launch {
            val response = transfertRepository.changeTransferEtat(ref, motif)
            restoredTransfert.value = response
        }
    }

    fun isNameValid(name: String): Boolean {
        return name.isNotBlank()
    }

    fun isNumValid(num: Int?): Boolean {
        return !( num==0 || num == null)
    }
}