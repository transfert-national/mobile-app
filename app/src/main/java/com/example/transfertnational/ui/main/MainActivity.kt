package com.example.transfertnational.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.transfertnational.R
import com.example.transfertnational.databinding.ActivityMainBinding
import com.example.transfertnational.repositories.UserRepository
import com.example.transfertnational.utils.Constants.Companion.CLIENT_ID
import com.example.transfertnational.utils.Constants.Companion.CLIENT_SECRET
import com.example.transfertnational.utils.SessionManager
import com.example.transfertnational.utils.Utils.toast
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var toggle: ActionBarDrawerToggle
     lateinit var binding: ActivityMainBinding
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController
    private lateinit var sessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sessionManager = SessionManager(this)
        toggle = ActionBarDrawerToggle(this, binding.drawerLayout, R.string.open, R.string.close)
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        navController = navHostFragment.navController

        binding.navView.setNavigationItemSelectedListener {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            if(sessionManager.fetchAuthToken().isNullOrEmpty())(
                    this.toast("accès refusé")
            )else {
                when (it.itemId) {
                    R.id.home -> {
                        navController.navigate(R.id.homeFragment)
                    }
                    R.id.logout -> {
                        logout()
                    }
                    else ->
                        navigateTo(it.itemId)
                }
            }
            true
        }
        binding.navView.itemIconTintList = null

    }

    private fun logout() {
        val userRepository = UserRepository()
            userRepository.logout(CLIENT_ID, sessionManager.fetchRefreshToken(), CLIENT_SECRET).enqueue(object :Callback<ResponseBody>{
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    if(response.isSuccessful && response!=null){
                        sessionManager.saveAuthToken(null)
                        sessionManager.saveRefreshToken(null)
                        this@MainActivity.toast(getString(R.string.success_logout))
                        navigateTo(R.id.loginFragment)
                    }
                    else
                        this@MainActivity.toast(getString(R.string.logout_failed))
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    this@MainActivity.toast(getString(R.string.verify_cnx))
                }

            })
    }


    private fun navigateTo(id: Int) {
        navController.popBackStack()
        navController.navigate(id)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item))
            return true
        return super.onOptionsItemSelected(item)

    }

    override fun onDestroy() {
        sessionManager.saveAuthToken(null)
        sessionManager.saveRefreshToken(null)
        super.onDestroy()
    }

}