package com.example.transfertnational.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.network.params.LoginInput
import com.example.transfertnational.network.responses.AccessToken
import com.example.transfertnational.network.responses.LoginResponse

import com.example.transfertnational.repositories.UserRepository
import com.example.transfertnational.utils.Constants.Companion.CLIENT_ID
import com.example.transfertnational.utils.Constants.Companion.CLIENT_SECRET
import com.example.transfertnational.utils.Constants.Companion.GRANT_TYPE
import com.example.transfertnational.utils.Constants.Companion.SCOPE
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class LoginViewModel : ViewModel() {


    var username: MutableLiveData<String> = MutableLiveData()
    var password: MutableLiveData<String> = MutableLiveData()
    var response: MutableLiveData<Response<AccessToken>> = MutableLiveData()

    private val userRepository = UserRepository()

    fun getAccessToken() {
        viewModelScope.launch {
            val result = userRepository.getAccessToken(
                CLIENT_ID,
                GRANT_TYPE,
                CLIENT_SECRET,
                SCOPE,
                username.value!!.trim(),
                password.value!!.trim()
            )
            response.value = result

        }
    }
    val valid = MutableLiveData(false)

    // A placeholder username validation check
    fun isUserNameValid(username: String): Boolean {
        return username.isNotBlank()
    }

    // A placeholder password validation check
    fun isPasswordValid(password: String): Boolean {
        return password.length > 3
    }
}