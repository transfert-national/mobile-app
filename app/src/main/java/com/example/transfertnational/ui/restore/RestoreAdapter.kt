package com.example.transfertnational.ui.restore

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.transfertnational.R
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.databinding.HistoryItemBinding
import com.example.transfertnational.databinding.TransfertItemRecyclerBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class RestoreAdapter(
    private val transfertList: ArrayList<Transfert>?,
    private val beneficiaryClients: ArrayList<BeneficiaryClient>?
) :
    RecyclerView.Adapter<RestoreAdapter.RestoreViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestoreViewHolder {
        val view =
            TransfertItemRecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RestoreViewHolder(view)
    }

    override fun onBindViewHolder(holder: RestoreViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return transfertList?.size ?: 0
    }

    inner class RestoreViewHolder(val binding: TransfertItemRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(position: Int) {
            val currentTransfert = transfertList?.get(position)
            val currentBeneficiaryClient =
                beneficiaryClients?.firstOrNull { beneficiaryClient -> beneficiaryClient.id == currentTransfert!!.idBeneficiaire }
            val format = SimpleDateFormat("dd/MM/yyy", Locale.ENGLISH)
            binding.datetransfert.text = format.format(currentTransfert!!.dateTransfert!!)
            binding.nom.text =
                currentBeneficiaryClient?.lastName?.uppercase() ?: "" + " " +
                        currentBeneficiaryClient?.firstName.toString()
                            .replaceFirstChar { it.uppercase() }
            (currentTransfert.montant.toString() + " DH").also { binding.montant.text = it }
            binding.button.setOnClickListener {
                val bundle = bundleOf(
                    "transfert" to currentTransfert,
                    "nameBenficiaryCLient" to binding.nom.text.toString()
                )
                Navigation.findNavController(binding.root)
                    .navigate(R.id.action_restoreFragment_to_validateRestoreFragment, bundle)
            }

        }
    }

}