package com.example.transfertnational.ui.restore

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.transfertnational.R
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.databinding.ValidateRestoreFragmentBinding
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.ui.transfert.VerifyOtpDialog
import com.example.transfertnational.utils.Constants.Companion.registrationNumber
import com.example.transfertnational.utils.Utils.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ValidateRestoreFragment : BaseFragment() {


    private lateinit var binding: ValidateRestoreFragmentBinding
    private val viewModel: ValidateRestoreViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ValidateRestoreFragmentBinding.inflate(inflater, container, false)
        activity.supportActionBar!!.show()
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.transfert.value = requireArguments().get("transfert") as Transfert
        viewModel.nomBeneficiary.value = requireArguments().getString("nameBenficiaryCLient")
        viewModel.getClient(registrationNumber)
        viewModel.clientCall.observe(viewLifecycleOwner) { response ->
            val clientResponse = response.body()
            if (response.code() == 200 && clientResponse != null) {
                viewModel.name.value =
                    clientResponse.lastName!!.uppercase() + " " + clientResponse.firstName!!.replaceFirstChar { it.uppercase() }
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        binding.motif.doOnTextChanged { text, _, _, _ ->
            if (!viewModel.isNameValid(binding.motif.text.toString()))
                binding.motif.error = getString(R.string.enter_motif)
        }

        val motifList = arrayListOf(
            "Soutien familial",
            "Epargne/investissement",
            "Cadeau",
            "Paiement de biens et de services",
            "Frais de dépassement",
            "Frais d’éducation",
            "Location/Hypothèque",
            "Aide de secours/Médicale",
            "Revenu d’un site internet",
            "Dépenses salariales",
            "Frais de loterie ou récompense/taxes",
            "Prêt",
            "Commerce sur internet",
            "Donation",
            "Autres (à préciser)"
        )
        val motifAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdownitem, R.id.beneficiary, motifList)
        binding.motifs.setAdapter(motifAdapter)
        binding.motifs.setOnItemClickListener { adapterView, view, i, l ->
            val item = adapterView!!.getItemAtPosition(i)
            if (item == motifList.last()) {
                viewModel.motif.value = ""
                binding.autre.visibility = View.VISIBLE
            }
            else{
                viewModel.motif.value = item.toString()
                binding.autre.visibility = View.GONE
            }

        }
        binding.generateOtp.setOnClickListener {
            binding.verify.visibility = View.VISIBLE
            viewModel.generateOtp().enqueue(object :Callback<Void>{
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    if (response.isSuccessful){
                        viewModel.isenabled.value = true
                    }
                    else
                        requireContext().toast("La génération de code à échoué, Veuillez reéssayez!")
                }
                override fun onFailure(call: Call<Void>, t: Throwable) {
                    requireContext().toast("Verifier votre connexion Internet")
                }
            })
        }
        binding.verify.setOnClickListener {

            viewModel.otp.value!!.clientId = registrationNumber
            openOtpdialog()

        }
        viewModel.verify.observe(viewLifecycleOwner) {
            if (it) {
                requireContext().toast("Le code est correct, Vous pouvez restituer votre transfert")
                binding.restituer.visibility = View.VISIBLE
                binding.verify.visibility = View.GONE
                binding.genLayout.visibility = View.GONE
            } else
                requireContext().toast("Le code que vous avez saisi est incorrect")
        }
        binding.restituer.setOnClickListener {
            if (viewModel.motif.value == null)
                requireContext().toast(getString(R.string.enter_motif))
            else
                viewModel.restoreTransfert(viewModel.transfert.value!!.ref,viewModel.motif.value)
        }
        viewModel.restoredTransfert.observe(viewLifecycleOwner){ response ->
            val transfertResponse = response.body()
            if (response.code() == 200 && transfertResponse != null) {
                requireContext().toast("Votre transfert a été restitué avec succés")
                findNavController().popBackStack()
                Navigation.findNavController(requireView())
                    .navigate(R.id.homeFragment)
            } else {
                requireContext().toast("Votre transfert n'a pas pu étre restitué")
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }

        }
        return binding.root
    }

    private fun openOtpdialog() {
        val verifyOtpRestoreDialog = VerifyOtpRestoreDialog()
        verifyOtpRestoreDialog.show(requireActivity().supportFragmentManager, "otp dialog")
    }



}