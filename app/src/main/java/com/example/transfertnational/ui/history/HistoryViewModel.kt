package com.example.transfertnational.ui.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.repositories.BeneficiaryClientRepository
import com.example.transfertnational.repositories.TransfertRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class HistoryViewModel : ViewModel() {

    val lastIsReached = MutableLiveData(false)
    private val transfertRepository = TransfertRepository()
    private val beneficiaryClientRepository = BeneficiaryClientRepository()
    val listTransferts: MutableLiveData<Response<List<Transfert>>> = MutableLiveData()
    val listBens: MutableLiveData<ArrayList<BeneficiaryClient>> = MutableLiveData(ArrayList())
    val beneficiaryClient = MutableLiveData<Response<BeneficiaryClient>>()


    fun getTransferts(idUser:String?){
        viewModelScope.launch {
            val response= transfertRepository.getTransfertsByUser(idUser)
            listTransferts.value = response
        }
    }

    fun getBeneficiaryClient(id: String?) {
        viewModelScope.launch {
           val response = beneficiaryClientRepository.getBeneficiaryClient(id)
            beneficiaryClient.value = response
        }
    }
}