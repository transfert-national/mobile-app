package com.example.transfertnational.ui.transfert

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.transfertnational.R
import com.example.transfertnational.databinding.AddBeneficiaryDialogBinding
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import java.util.*
import kotlin.random.Random


class AddBeneficiaryDialog: DialogFragment() {

    lateinit var viewModel: AddBeneficiaryViewModel
    lateinit var binding:AddBeneficiaryDialogBinding
    var emailIsValid = false
    var firstNameIsValid = false
    var lastNameIsValid = false
    var phoneIsValid = false


    @SuppressLint("ResourceAsColor")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        binding= DataBindingUtil.inflate(
            inflater,
            R.layout.add_beneficiary_dialog, null, false
        )
        viewModel = ViewModelProvider(this).get(AddBeneficiaryViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        val clientId = requireArguments().getString("clientId")
        with(binding) {
            emailBenef.doOnTextChanged { text, _, _, _ ->
                viewModel?.email?.value = text.toString()
                if (!emailIsValid)
                    emailBenef.error = getString(R.string.invalid_email)
            }
            nameBenef.doOnTextChanged { text, _, _, _ ->
                viewModel?.lastName?.value = text.toString()
                if (!lastNameIsValid)
                    nameBenef.error = getString(R.string.invalid_lastname)
            }
            phoneBenef.doOnTextChanged { text, _, _, _ ->
                viewModel?.phoneNumber?.value = text.toString()
                if (!phoneIsValid)
                    phoneBenef.error = getString(R.string.invalid_phone)

            }
            firstnameBenef.doOnTextChanged { text, _, _, _ ->
                viewModel?.firstName?.value = text.toString()
                if (!firstNameIsValid)
                    firstnameBenef.error = getString(R.string.invalid_firstname)
            }
        }

        val formIsValid : Flow<Boolean> = combine(
            viewModel.email,
            viewModel.firstName,
            viewModel.lastName,
            viewModel.phoneNumber
        ) { email, firstName, lastName, phoneNumber ->
            emailIsValid = viewModel.isEmailValid(email)
            firstNameIsValid = viewModel.isNameValid(firstName)
            lastNameIsValid = viewModel.isNameValid(lastName)
            phoneIsValid = viewModel.isNameValid(phoneNumber)
            return@combine emailIsValid && firstNameIsValid && lastNameIsValid && phoneIsValid
        }
        lifecycleScope.launch {
            formIsValid.collect {  binding.yes.apply {
                backgroundTintList = ColorStateList.valueOf(
                     if(it) R.color.dark_blue
                else Color.GRAY
                )
                backgroundTintMode = PorterDuff.Mode.SRC_ATOP
                isEnabled = it
            }
            }
        }

        binding.no.setOnClickListener{
            this.dismiss()
        }
        binding.yes.setOnClickListener{
            viewModel.beneficiaryClient.value!!.id = UUID.randomUUID().toString()
            viewModel.beneficiaryClient.value!!.clientId = clientId
            viewModel.addBeneficiaryClient(viewModel.beneficiaryClient.value!!)
        }
        viewModel.beneficiaryClientCall.observe(this) { response->
            val responseBeneficiaryClient = response.body()
            if (response.isSuccessful && responseBeneficiaryClient!= null){
                dismiss()
                findNavController()
                    .navigate(R.id.action_transferMoneyFragment_self)
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setView(binding.root)
                .setTitle("Ajouter un bénéficiare")
            builder.create()

        }?: throw IllegalStateException("Activity cannot be null")
    }

}