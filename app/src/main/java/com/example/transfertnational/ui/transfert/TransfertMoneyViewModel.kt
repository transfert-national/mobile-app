package com.example.transfertnational.ui.transfert

import android.util.Log
import android.widget.ArrayAdapter
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.R
import com.example.transfertnational.data.model.*
import com.example.transfertnational.repositories.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class TransfertMoneyViewModel : ViewModel() {


    private val clientRepository = ClientRepository()
    private val walletRepository = WalletRepository()
    private val transfertRepository = TransfertRepository()
    private val beneficiaryClientRepository = BeneficiaryClientRepository()
    private val otpRepository = OtpRepository()


    val wallet = MutableLiveData<Wallet>()
    val walletCall = MutableLiveData<Response<Wallet>>()
    val client = MutableLiveData<Client>()
    val clientCall = MutableLiveData<Response<Client>>()
    val name = MutableLiveData<String>()
    val walletCredit = MutableLiveData<String>()
    val beneficiaryClients = MutableLiveData<Response<List<BeneficiaryClient>>>()
    val beneficiaries = MutableLiveData<List<BeneficiaryClient>>()
    val selectedBeneficiaryClient = MutableLiveData<BeneficiaryClient>()
    val transfert = MutableLiveData(Transfert())
    var transferedItem = MutableLiveData<Response<Transfert>>()
    val otp = MutableLiveData(Otp())
    var verify = MutableLiveData<Boolean>()
    var isenabled = MutableLiveData<Boolean>(false)
    val frais = MutableStateFlow("")
    val montant= MutableStateFlow(0.0)


    fun clearall(){
        verify = MutableLiveData<Boolean>()
        transferedItem = MutableLiveData<Response<Transfert>>()
    }

    fun getClient(registrationNumber: String?){
        viewModelScope.launch {
            val response =
                clientRepository.getClientByRegistrationNumber(registrationNumber)
            clientCall.value = response
        }


    }
    fun getWallet(registrationNumber: String?){
        viewModelScope.launch {
            val response = walletRepository.getWalletByClient(registrationNumber)
            walletCall.value = response
        }
    }
    fun getBeneficiaryClients(registrationNumber: String?){
        viewModelScope.launch {
            val response = beneficiaryClientRepository.getBeneficiaryClientsByIdClient(registrationNumber)
            beneficiaryClients.value = response
        }
    }
    fun generateOtp(): Call<Void> {
        return otpRepository.generateOtp()
    }
    fun verifyOtp(otp:Otp?){
        viewModelScope.launch {
            val response = otpRepository.verifyOtp(otp)
            verify.value = response
        }
    }
    fun makeTransfert(transfert: Transfert){
        viewModelScope.launch {
            val response = transfertRepository.makeTransferFromCompteDebit(transfert)
            transferedItem.value = response
        }
    }
    /*val valid = MediatorLiveData<Boolean>().apply {
        addSource(beneficiaryClient) {
            searchBeneficiaryClient(it)
            value = true
        }
    }*/


    fun isBenValid(beneficiaryClient: BeneficiaryClient): Boolean {
        return !beneficiaryClient.equals(null)
    }
    fun isMontantValid(montant: Double): Boolean {
        return montant.toInt() != 0
    }

    fun isNameValid(name: String): Boolean {
        return name.isNotBlank()
    }
    fun isNumValid(num: Int?): Boolean {
        return !( num==0 || num == null)
    }

}