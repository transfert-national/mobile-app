package com.example.transfertnational.ui.wallet

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.example.transfertnational.R
import com.example.transfertnational.databinding.DialogAddMoneyToWalletBinding

class AddMoneyToWalletDialog: DialogFragment() {

    lateinit var binding:DialogAddMoneyToWalletBinding
    private val viewModel: AddMoneyToWalletViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        binding= DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_add_money_to_wallet, null, false
        )
        binding.viewModel = viewModel

        binding.no.setOnClickListener{
            this.dismiss()
        }
        binding.yes.setOnClickListener{
            //listener!!.confirm(true)
            viewModel.confirm.value = true
            dismiss()
        }
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setView(binding.root)
                .setTitle("Confirmer?")

            builder.create()
        }?: throw IllegalStateException("Activity cannot be null")

    }
}