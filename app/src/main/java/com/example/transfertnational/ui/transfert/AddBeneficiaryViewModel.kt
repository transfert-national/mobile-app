package com.example.transfertnational.ui.transfert

import android.util.Patterns
import android.widget.EditText
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.R
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.repositories.BeneficiaryClientRepository
import com.example.transfertnational.repositories.OtpRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import retrofit2.Response

class AddBeneficiaryViewModel :ViewModel(){

    private val beneficiaryClientRepository = BeneficiaryClientRepository()

    var beneficiaryClient = MutableLiveData(BeneficiaryClient())
    var beneficiaryClientCall = MutableLiveData<Response<BeneficiaryClient>>()

     val email = MutableStateFlow("")
     val firstName = MutableStateFlow("")
     val lastName = MutableStateFlow("")
     val phoneNumber = MutableStateFlow("")


    fun addBeneficiaryClient(beneficiary : BeneficiaryClient ){
        viewModelScope.launch {
            val response = beneficiaryClientRepository.addBeneficiaryClient(beneficiary)
            beneficiaryClientCall.value= response
        }
    }
    fun isEmailValid(username: String): Boolean {
        return  Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }
    fun isNameValid(username: String): Boolean {
        return username.isNotBlank()
    }
    fun setError(editText: EditText, string: String){
        editText.error = string
    }
}