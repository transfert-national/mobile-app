package com.example.transfertnational.ui.login

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.auth0.android.jwt.JWT
import com.example.transfertnational.R
import com.example.transfertnational.databinding.FragmentLoginBinding

import com.example.transfertnational.utils.SessionManager
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.utils.Constants.Companion.registrationNumber
import com.example.transfertnational.utils.Utils.toast

class LoginFragment : BaseFragment() {


    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: FragmentLoginBinding
    private lateinit var sessionManager: SessionManager



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        if (activity.supportActionBar!!.isShowing)
            activity.supportActionBar!!.hide()
        activity.binding.navView.visibility = View.GONE
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        binding.viewmodel = loginViewModel
        sessionManager = SessionManager(requireContext())
        var validUsername = false
        var validPassword = false
        loginViewModel.username.observe(viewLifecycleOwner){
            if (!loginViewModel.isUserNameValid(it))
            {
                binding.username.error = getString(R.string.invalid_username)
                validUsername = false
            }
            else
                validUsername = true
            loginViewModel.valid.value = validUsername && validPassword
        }
        loginViewModel.password.observe(viewLifecycleOwner){
           if (!loginViewModel.isPasswordValid(it) )
           {
               binding.password.error = getString(R.string.invalid_password)
               validPassword = false
           }
           else
               validPassword = true
            loginViewModel.valid.value = validUsername && validPassword
        }
        binding.login.setOnClickListener {
                binding.loading.visibility = View.VISIBLE
                loginViewModel.getAccessToken()
            it.isEnabled = false
        }
        loginViewModel.response.observe(viewLifecycleOwner) { response ->
            val loginResponse = response.body()

            if (response.code() == 200 && loginResponse != null) {
                sessionManager.saveAuthToken(loginResponse.accessToken)
                sessionManager.saveRefreshToken(loginResponse.refreshToken)
                decode()
                findNavController().popBackStack()
                Navigation.findNavController(view)
                    .navigate(R.id.homeFragment)
            } else {
                requireContext().toast(getString(R.string.login_failed))
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.errorBody()}"
                )
            }
            loginViewModel.valid.value = true
            binding.loading.visibility = View.GONE
        }

    }

    private fun decode() {
        val jwt = JWT(sessionManager.fetchAuthToken())
        registrationNumber = jwt.getClaim("registrationNumber").asString().toString()
        Log.i("registrationNumber", registrationNumber)
    }


    override fun onDetach() {
        super.onDetach()
        activity.binding.navView.visibility = View.VISIBLE
    }


}