package com.example.transfertnational.ui.history

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.transfertnational.R
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.databinding.FragmentHistoryBinding
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.utils.Constants.Companion.registrationNumber
import java.time.LocalDateTime


class HistoryFragment : BaseFragment() {


    private lateinit var binding: FragmentHistoryBinding
    private lateinit var viewModel: HistoryViewModel
    private  var transferts= ArrayList<Transfert>()
    private  var beneficiaryClients = ArrayList<BeneficiaryClient>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity.supportActionBar!!.show()
        if (activity.supportActionBar!!.isShowing) {
            activity.supportActionBar!!.title = getText(R.string.history)
        }
        binding = FragmentHistoryBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(HistoryViewModel::class.java)
        binding.lifecycleOwner = this

        viewModel.getTransferts(registrationNumber)
        var sizeIds = 0
        viewModel.listTransferts.observe(viewLifecycleOwner) { response ->
            val responseTransferts = response.body()

            if (response.isSuccessful && !responseTransferts.isNullOrEmpty()) {
                transferts = responseTransferts as ArrayList<Transfert>
                transferts =ArrayList( transferts.sortedByDescending { transfert -> transfert.dateTransfert })
                val beneficiaryClientsIds = responseTransferts.map { transfert -> transfert.idBeneficiaire  }.distinct()
                sizeIds = beneficiaryClientsIds.size
                beneficiaryClientsIds.forEach { id ->
                        viewModel.apply {
                            getBeneficiaryClient(id!!)
                            beneficiaryClient.observe(viewLifecycleOwner){ response ->
                                val benResponse = response.body()
                                if (response.code() == 200 && benResponse != null ) {
                                    listBens.value?.add(benResponse)
                                    if (sizeIds == listBens.value?.distinct()?.size)
                                        this.lastIsReached.value = true
                                } else {
                                    Log.i(
                                        "request error",
                                        "error code ${response.code()} message ${response.message()}"
                                    )
                                }

                            }
                        }
                    }
            }

        }
        viewModel.lastIsReached.observe(viewLifecycleOwner){
            if (it){
                beneficiaryClients = viewModel.listBens.value!!
                binding.recycler.layoutManager =
                    LinearLayoutManager(view?.context, RecyclerView.VERTICAL, false)
                binding.recycler.addItemDecoration(
                    DividerItemDecoration(
                        context,
                        DividerItemDecoration.VERTICAL
                    )
                )
                val adapter = HistoryAdapter(
                    transferts as ArrayList<Transfert>?,
                    beneficiaryClients
                )
                binding.recycler.adapter = adapter
            }
        }
        return binding.root
    }


}