package com.example.transfertnational.ui.welcome

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.transfertnational.R
import com.example.transfertnational.databinding.WelcomeFragmentBinding
import com.example.transfertnational.utils.SessionManager
import com.example.transfertnational.ui.base.BaseFragment

class WelcomeFragment : BaseFragment() {

    private lateinit var sessionManager : SessionManager

    private lateinit var binding: WelcomeFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = WelcomeFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        sessionManager = SessionManager(requireContext())
        if (activity.supportActionBar!!.isShowing)
            activity.supportActionBar!!.hide()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).postDelayed({

           // if (sessionManager.fetchAuthToken().isNullOrEmpty()){
                Navigation.findNavController(view)
                    .navigate(R.id.action_welcomeFragment_to_loginFragment)
           /* } else{
                Navigation.findNavController(view)
                    .navigate(R.id.action_welcomeFragment_to_homeFragment)
        }*/
        }, 3000)

    }

}