package com.example.transfertnational.ui

import android.text.TextUtils
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseMethod

object Converter {
    @InverseMethod("convertStringToLong")
    @JvmStatic
    fun convertLongToString(value: String): Long? {
        if (TextUtils.isEmpty(value) || !TextUtils.isDigitsOnly(value)) {
            return null
        }
        return value.toLongOrNull()
    }

    @JvmStatic
    fun convertStringToLong(value: Long?): String {
        return value?.toString() ?: ""
    }
    @InverseMethod("convertStringToDouble")
    @JvmStatic
    fun convertDoubleToString(value: String): Double? {
        if (TextUtils.isEmpty(value) || !TextUtils.isDigitsOnly(value)) {
            return null
        }
        return value.toDouble()
    }

    @JvmStatic
    fun convertStringToDouble(value: Double?): String {
        return value?.toString() ?: ""
    }

    @InverseMethod(value="convertStringToInt")
    @JvmStatic
    fun  convertIntToString(value:Int?) :String{
        return (value!!).toString()
    }
    @JvmStatic
    fun convertStringToInt(value: Int?): String {
        return value?.toString() ?: ""
    }
}