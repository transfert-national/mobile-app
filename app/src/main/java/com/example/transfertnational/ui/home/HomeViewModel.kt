package com.example.transfertnational.ui.home

import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.data.model.Account
import com.example.transfertnational.data.model.Client
import com.example.transfertnational.data.model.Wallet
import com.example.transfertnational.repositories.AccountRepository
import com.example.transfertnational.repositories.ClientRepository
import com.example.transfertnational.repositories.WalletRepository
import com.example.transfertnational.utils.Utils.toast
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {

    private val clientRepository = ClientRepository()
    private val walletRepository = WalletRepository()
    private val accountRepository = AccountRepository()

    val wallet = MutableLiveData<Wallet>()
    val walletCall = MutableLiveData<Response<Wallet>>()
    val client = MutableLiveData<Client>()
    val clientCall = MutableLiveData<Response<Client>>()
    val account = MutableLiveData<Account>()
    val accountCall = MutableLiveData<Response<Account>>()
    val name = MutableLiveData<String>()
    val walletCredit = MutableLiveData<String>()
    val accountCredit = MutableLiveData<String>()

    fun getClient(registrationNumber: String?){
        viewModelScope.launch {
            val response =
            clientRepository.getClientByRegistrationNumber(registrationNumber)
            clientCall.value = response
        }


    }
    fun getWallet(registrationNumber: String?){
        viewModelScope.launch {
            val response = walletRepository.getWalletByClient(registrationNumber)
            walletCall.value = response
        }
    }
    fun getAccount(clientId: String?){
        viewModelScope.launch {
            val response = accountRepository.getAccountByClientId(clientId)
            accountCall.value = response
        }
    }

}