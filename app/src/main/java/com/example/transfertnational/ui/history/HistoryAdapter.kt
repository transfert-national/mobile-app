package com.example.transfertnational.ui.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.transfertnational.R
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.databinding.HistoryItemBinding
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryAdapter(private val transfertList: ArrayList<Transfert>?,private val beneficiaryClients: ArrayList<BeneficiaryClient>?):
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view = HistoryItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return HistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return transfertList?.size ?: 0
    }
    inner class HistoryViewHolder(val binding:HistoryItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            val currentTransfert = transfertList?.get(position)
            val format = SimpleDateFormat("dd/MM/yyy", Locale.ENGLISH)
            val currentBeneficiaryClient =
                beneficiaryClients?.firstOrNull { beneficiaryClient -> beneficiaryClient.id == currentTransfert!!.idBeneficiaire }
            binding.datetransfert.text = format.format(currentTransfert!!.dateTransfert!!)
            binding.nameben.text =
                currentBeneficiaryClient?.lastName.toString().uppercase() + " " +
                        currentBeneficiaryClient?.firstName.toString().replaceFirstChar { it.uppercase() }
            (currentTransfert.montant.toString()+" DH").also { binding.montant.text = it }
            binding.reference.text = currentTransfert.ref
        }
    }

}