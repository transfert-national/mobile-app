package com.example.transfertnational.ui.restore

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.data.enum.Etat
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.repositories.BeneficiaryClientRepository
import com.example.transfertnational.repositories.TransfertRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response

class RestoreViewModel : ViewModel() {

    private val transfertRepository = TransfertRepository()
    private val beneficiaryClientRepository = BeneficiaryClientRepository()
    val ref = MutableLiveData<String>()
    val transfertCall = MutableLiveData<Response<Transfert>>()
    val transfert = MutableLiveData<Transfert>()
    val beneficiaryClient = MutableLiveData<Response<BeneficiaryClient>>()
    val oneBeneficiaryClient = MutableLiveData<Response<BeneficiaryClient>>()
    val listTransferts: MutableLiveData<Response<List<Transfert>>> = MutableLiveData()
    val nomBeneficiary = MutableLiveData<String>()
    val lastIsReached = MutableLiveData(false)
    val listBens: MutableLiveData<ArrayList<BeneficiaryClient>> = MutableLiveData(ArrayList())


    fun getBeneficiaryClient(id: String?) {
        viewModelScope.launch {
            val response = beneficiaryClientRepository.getBeneficiaryClient(id)
            beneficiaryClient.value = response
        }
    }
    fun getOneBeneficiaryClient(id: String?) {
        viewModelScope.launch {
            val response = beneficiaryClientRepository.getBeneficiaryClient(id)
            oneBeneficiaryClient.value = response
        }
    }

    fun getTransfertByref(){
        viewModelScope.launch {
            val response= transfertRepository.getTransfertByRef(ref.value)
            transfertCall.value = response
        }
    }
    fun getTransfertsByEtat(id:String?){
        viewModelScope.launch {
            val response = transfertRepository.getTransfertByEtatAndUser(Etat.A_SERVIR,id)
            listTransferts.value = response
        }
    }
    fun isNameValid(name: String): Boolean {
        return name.isNotBlank()
    }

}