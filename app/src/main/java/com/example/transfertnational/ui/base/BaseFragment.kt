package com.example.transfertnational.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.transfertnational.R
import com.example.transfertnational.ui.main.MainActivity

open class BaseFragment :Fragment() {

    lateinit var activity : MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.activity = getActivity() as MainActivity

    }


}