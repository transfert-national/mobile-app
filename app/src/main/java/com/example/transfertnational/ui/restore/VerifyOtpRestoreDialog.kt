package com.example.transfertnational.ui.restore

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.example.transfertnational.R
import com.example.transfertnational.databinding.VerifyOtpDialogBinding
import com.example.transfertnational.utils.Utils.toast

class VerifyOtpRestoreDialog : DialogFragment() {


    lateinit var binding: VerifyOtpDialogBinding
    private val viewModel: ValidateRestoreViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = requireActivity().layoutInflater
        binding= DataBindingUtil.inflate(
            inflater,
            R.layout.verify_otp_dialog, null, false
        )

        binding.code.doOnTextChanged { text, _, _, _ ->
            if (!viewModel.isNameValid(text.toString()))
                binding.code.error = getString(R.string.enter_code)
        }
        binding.no.setOnClickListener{
            this.dismiss()
        }
        binding.yes.setOnClickListener{
            if (binding.code.text.isNullOrBlank()){
                requireContext().toast(getString(R.string.enter_code))
            }else
            {
                viewModel.otp.value!!.codePin = Integer.parseInt(binding.code.text.toString())
                viewModel.verifyOtp(viewModel.otp.value)
                dismiss()
            }
        }
        return activity?.let {
            val builder = AlertDialog.Builder(it)


            builder.setView(binding.root)
                .setTitle("Verifier le code")
            builder.create()

        }?: throw IllegalStateException("Activity cannot be null")
    }
}