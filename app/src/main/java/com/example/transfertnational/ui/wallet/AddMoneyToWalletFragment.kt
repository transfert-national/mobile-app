package com.example.transfertnational.ui.wallet

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.transfertnational.R
import com.example.transfertnational.databinding.FragmentAddMoneyToWalletBinding
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.utils.Constants.Companion.registrationNumber


class AddMoneyToWalletFragment : BaseFragment()  {

    private val viewModel: AddMoneyToWalletViewModel by activityViewModels()
    private lateinit var binding: FragmentAddMoneyToWalletBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddMoneyToWalletBinding.inflate(inflater, container, false)
        activity.supportActionBar!!.show()
        if (activity.supportActionBar!!.isShowing) {
            activity.supportActionBar!!.title = getText(R.string.My_account)
        }
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        binding.opendiag.setOnClickListener {
            viewModel.apply {
                newAccountCredit.value = account.value!!.balance.minus(money.value!!).toString() + " DH"
                newWalletCredit.value = wallet.value!!.balance?.plus(money.value!!).toString() + " DH"
            }
            opendialog()

        }
        viewModel.money.observe(viewLifecycleOwner){
            if (!viewModel.isMontantValid(it))
            {
                binding.montantTransfert.error = getString(R.string.invalid_montant)
                viewModel.valid.value = false
            }
            else
                viewModel.valid.value = true
        }
        binding.lifecycleOwner = this
        viewModel.getClient(registrationNumber)
        viewModel.clientCall.observe(viewLifecycleOwner) { response ->
            val clientResponse = response.body()
            if (response.code() == 200 && clientResponse != null) {
                viewModel.client.value = clientResponse
                viewModel.name.value =
                    clientResponse.lastName!!.uppercase() + " " + clientResponse.firstName!!.replaceFirstChar { it.uppercase() }
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        viewModel.getWallet(registrationNumber)
        viewModel.walletCall.observe(viewLifecycleOwner) { response ->
            val walletResponse = response.body()
            if (response.code() == 200 && walletResponse != null) {
                viewModel.wallet.value  = walletResponse
                viewModel.walletCredit.value = walletResponse.balance.toString() + " DH"
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        viewModel.getAccount(registrationNumber)
        viewModel.accountCall.observe(viewLifecycleOwner) { response ->
            val accountResponse = response.body()
            if (response.code() == 200 && accountResponse != null) {
                viewModel.account.value = accountResponse
                viewModel.accountCredit.value = accountResponse.balance.toString() + " DH"
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        viewModel.confirm.observe(viewLifecycleOwner){
            if (it){
                viewModel.addMoneyToWallet(registrationNumber,binding.montantTransfert.text.toString().toDouble())
                viewModel.confirm.value = false
            }
        }

        return binding.root
    }

    private fun opendialog() {
        val addMoneyToWalletDialog = AddMoneyToWalletDialog()
        addMoneyToWalletDialog.show(requireActivity().supportFragmentManager,"walletdialog")


    }


}