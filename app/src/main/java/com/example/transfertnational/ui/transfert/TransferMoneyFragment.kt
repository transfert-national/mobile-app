package com.example.transfertnational.ui.transfert

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.graphics.drawable.toDrawable
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.transfertnational.R
import com.example.transfertnational.data.enum.TypeFrais
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.databinding.FragmentTransferMoneyBinding
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.utils.Constants.Companion.registrationNumber
import com.example.transfertnational.utils.Utils.toast
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.xml.validation.Validator

class TransferMoneyFragment : BaseFragment() {

    private lateinit var binding: FragmentTransferMoneyBinding
    private val viewModel: TransfertMoneyViewModel by activityViewModels()
    var benIsValid = false
    var montantIsValid = false
    var fraisIsValid = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTransferMoneyBinding.inflate(inflater, container, false)
        activity.supportActionBar!!.show()
        if (activity.supportActionBar!!.isShowing) {
            activity.supportActionBar!!.title = getText(R.string.money_exchange)
        }
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        viewModel.getBeneficiaryClients(registrationNumber)
        viewModel.beneficiaryClients.observe(viewLifecycleOwner) { response ->
            val beneficiaryClientResponse = response.body()
            if (response.code() == 200 && beneficiaryClientResponse != null) {
                viewModel.beneficiaries.value = beneficiaryClientResponse
                val arrayAdapter = ArrayAdapter(
                    requireContext(),
                    R.layout.dropdownitem,
                    R.id.beneficiary,
                    beneficiaryClientResponse as ArrayList
                )
                binding.autoCompleteTextView6.setAdapter(arrayAdapter)
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }

        binding.autoCompleteTextView6.setOnItemClickListener { adapterView, view, i, l ->
            val item = adapterView!!.getItemAtPosition(i) as BeneficiaryClient
            viewModel.selectedBeneficiaryClient.value = item
            viewModel.transfert.value!!.idBeneficiaire = item.id
        }
        viewModel.selectedBeneficiaryClient.observe(viewLifecycleOwner) {
            if (!viewModel.isBenValid(it)) {
                binding.textInputLayoutb.error = getString(R.string.invalid_ben)
                benIsValid = false
            } else
                benIsValid = true

        }
        with(binding) {
            frais.doOnTextChanged { text, _, _, _ ->
                viewModel.frais.value = text.toString()
                if (!fraisIsValid)
                    binding.textInputLayoutf.error = getString(R.string.invalid_value)
            }
            montantTransfert.doOnTextChanged { text, _, _, _ ->
                viewModel.montant.value = convertDoubleToString(text.toString())
                if (!montantIsValid)
                    binding.montantTransfert.error = getString(R.string.invalid_montant)
            }
        }
        val formIsValid: Flow<Boolean> = combine(
            viewModel.montant,
            viewModel.frais
        ) {  montant, frais ->
            montantIsValid = viewModel.isMontantValid(montant)
            fraisIsValid = viewModel.isNameValid(frais)
            return@combine montantIsValid && fraisIsValid && benIsValid
        }
        lifecycleScope.launch {
            formIsValid.collect {
                binding.transferMoney.isEnabled = it
            }
        }


        binding.frais.setOnItemClickListener { adapterView, view, i, l ->
            viewModel.transfert.value!!.typeFrais = TypeFrais.values()[i]
        }


        binding.add.setOnClickListener { opendialog(registrationNumber!!) }

        val fraisList = arrayListOf(
            "Frais à la charge du client d’donneur d’ordre",
            "Frais à la charge du client bénéficiaire",
            "Frais partagés entre les clients (Donneur d’ordre et bénéficiaire)"
        )
        val fraisAdapter =
            ArrayAdapter(requireContext(), R.layout.dropdownitem, R.id.beneficiary, fraisList)
        binding.frais.setAdapter(fraisAdapter)

        viewModel.getClient(registrationNumber)
        viewModel.clientCall.observe(viewLifecycleOwner) { response ->
            val clientResponse = response.body()
            if (response.code() == 200 && clientResponse != null) {
                viewModel.client.value = clientResponse
                viewModel.name.value =
                    clientResponse.lastName!!.uppercase() + " " + clientResponse.firstName!!.replaceFirstChar { it.uppercase() }
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        viewModel.getWallet(registrationNumber)
        viewModel.walletCall.observe(viewLifecycleOwner) { response ->
            val walletResponse = response.body()
            if (response.code() == 200 && walletResponse != null) {
                viewModel.wallet.value = walletResponse
                viewModel.walletCredit.value = walletResponse.balance.toString() + " DH"
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        binding.generateOtp.setOnClickListener {
            binding.verify.visibility = VISIBLE
            viewModel.generateOtp().enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    if (response.isSuccessful) {
                        viewModel.isenabled.value = true
                    } else
                        requireContext().toast(getString(R.string.generate_otp_failed))
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    requireContext().toast(getString(R.string.verify_cnx))
                }
            })
        }
        binding.verify.setOnClickListener {

            viewModel.otp.value!!.clientId = registrationNumber
            openOtpdialog()

        }
        viewModel.verify.observe(viewLifecycleOwner) {
            if (it) {
                requireContext().toast("Le code est correct, Vous pouvez effectuer votre transfert")
                binding.transferMoney.visibility = VISIBLE
                binding.verify.visibility = GONE
                binding.genLayout.visibility = GONE
            } else
                requireContext().toast("Ton code est incorrect")
        }
        binding.transferMoney.setOnClickListener {
            viewModel.transfert.value!!.idDonneur = registrationNumber
            viewModel.transfert.value!!.idUser = registrationNumber
            viewModel.makeTransfert(viewModel.transfert.value!!)
        }
        viewModel.transferedItem.observe(viewLifecycleOwner) { response ->
            val transfertResponse = response.body()
            if (response.code() == 200 && transfertResponse != null) {
                requireContext().toast("Votre transfert a été éffectué avec succés")
                findNavController().popBackStack()
                viewModel.clearall()
                Navigation.findNavController(requireView())
                    .navigate(R.id.homeFragment)
            } else {
                requireContext().toast("Votre transfert n'a pas pu étre éffectué")
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }

        }
        return binding.root

    }

    private fun opendialog(clientId: String) {
        val args = bundleOf("clientId" to clientId)
        val addBeneficiaryDialog = AddBeneficiaryDialog()
        addBeneficiaryDialog.arguments = args
        addBeneficiaryDialog.show(requireActivity().supportFragmentManager, "addbenef dialog")
    }

    private fun openOtpdialog() {
        val verifyOtpDialog = VerifyOtpDialog()
        verifyOtpDialog.show(requireActivity().supportFragmentManager, "otp dialog")
    }

    private fun convertDoubleToString(value: String): Double {
        if (TextUtils.isEmpty(value) || !TextUtils.isDigitsOnly(value)) {
            return 0.0
        }
        return value.toDouble()
    }

}