package com.example.transfertnational.ui.home

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.example.transfertnational.R
import com.example.transfertnational.databinding.HomeFragmentBinding
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.ui.login.LoginFragment
import com.example.transfertnational.utils.Constants.Companion.registrationNumber

class HomeFragment : BaseFragment() {


    private lateinit var homeviewModel: HomeViewModel
    private lateinit var binding: HomeFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        activity.supportActionBar!!.show()
        if (activity.supportActionBar!!.isShowing) {
            activity.supportActionBar!!.title = getText(R.string.home)
        }
        homeviewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.viewmodel = homeviewModel
        binding.lifecycleOwner = this
        homeviewModel.getClient(registrationNumber)
        homeviewModel.clientCall.observe(viewLifecycleOwner) { response ->
            val clientResponse = response.body()
            if (response.code() == 200 && clientResponse != null) {
                homeviewModel.client.value = clientResponse
                homeviewModel.name.value =
                    clientResponse.lastName!!.uppercase() + " " + clientResponse.firstName!!.replaceFirstChar { it.uppercase() }
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        homeviewModel.getWallet(registrationNumber)
        homeviewModel.walletCall.observe(viewLifecycleOwner) { response ->
            val walletResponse = response.body()
            if (response.code() == 200 && walletResponse != null) {
                homeviewModel.wallet.value  = walletResponse
                homeviewModel.walletCredit.value = walletResponse.balance.toString() + " DH"
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        homeviewModel.getAccount(registrationNumber)
        homeviewModel.accountCall.observe(viewLifecycleOwner) { response ->
            val accountResponse = response.body()
            if (response.code() == 200 && accountResponse != null) {
                homeviewModel.account.value = accountResponse
                homeviewModel.accountCredit.value = accountResponse.balance.toString() + " DH"
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }

        return binding.root
    }




}