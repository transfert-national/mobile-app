package com.example.transfertnational.ui.wallet

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.transfertnational.data.model.Account
import com.example.transfertnational.data.model.Client
import com.example.transfertnational.data.model.Wallet
import com.example.transfertnational.network.params.WithdrawInput
import com.example.transfertnational.repositories.AccountRepository
import com.example.transfertnational.repositories.ClientRepository
import com.example.transfertnational.repositories.WalletRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class AddMoneyToWalletViewModel : ViewModel() {

    private val clientRepository = ClientRepository()
    private val walletRepository = WalletRepository()
    private val accountRepository = AccountRepository()

    val wallet = MutableLiveData<Wallet>()
    val walletCall = MutableLiveData<Response<Wallet>>()
    val client = MutableLiveData<Client>()
    val clientCall = MutableLiveData<Response<Client>>()
    val account = MutableLiveData<Account>()
    val accountCall = MutableLiveData<Response<Account>>()
    val name = MutableLiveData<String>()
    val walletCredit = MutableLiveData<String>()
    val accountCredit = MutableLiveData<String>()
    val newWalletCredit = MutableLiveData<String>()
    val newAccountCredit = MutableLiveData<String>()
    val money = MutableLiveData<Double>()
    val confirm= MutableLiveData<Boolean>(false)
    var moneyVariable =
        object: ObservableField<String>() {
            override fun set(value: String?) {
                super.set(value)
                // a value has been set
                money.value = value!!.toDoubleOrNull() ?: money.value
            }
        }

    fun getClient(registrationNumber: String?){
        viewModelScope.launch {
            val response =
                clientRepository.getClientByRegistrationNumber(registrationNumber)
            clientCall.value = response
        }


    }
    fun getWallet(registrationNumber: String?){
        viewModelScope.launch {
            val response = walletRepository.getWalletByClient(registrationNumber)
            walletCall.value = response
        }
    }
    fun getAccount(clientId: String?){
        viewModelScope.launch {
            val response = accountRepository.getAccountByClientId(clientId)
            accountCall.value = response
        }
    }
    fun addMoneyToWallet(registrationNumber: String?,amount:Double){
        val withdrawInput = WithdrawInput(registrationNumber!!,amount)
        viewModelScope.launch {
            val response1 = walletRepository.depositByClientId(registrationNumber, amount)
            val response2 = accountRepository.withdrawByRegistrationNumber(withdrawInput)
            walletCall.value = response1
            accountCall.value = response2
        }
    }
    val valid = MutableLiveData(false)

    // A placeholder validation check
    fun isMontantValid(montant: Double): Boolean {
        return montant.toInt() != 0
    }

}