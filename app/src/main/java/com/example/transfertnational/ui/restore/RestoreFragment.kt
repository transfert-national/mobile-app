package com.example.transfertnational.ui.restore

import android.os.Build
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.transfertnational.R
import com.example.transfertnational.data.enum.Etat
import com.example.transfertnational.data.model.BeneficiaryClient
import com.example.transfertnational.data.model.Transfert
import com.example.transfertnational.databinding.RestoreFragmentBinding
import com.example.transfertnational.ui.base.BaseFragment
import com.example.transfertnational.utils.Constants.Companion.registrationNumber
import com.example.transfertnational.utils.Utils.toast
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList

class RestoreFragment : BaseFragment() {


    private lateinit var binding: RestoreFragmentBinding
    private lateinit var viewModel: RestoreViewModel
    private var transferts = ArrayList<Transfert>()
    private var beneficiaryClients = ArrayList<BeneficiaryClient>()


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = RestoreFragmentBinding.inflate(inflater, container, false)
        activity.supportActionBar!!.show()
        if (activity.supportActionBar!!.isShowing) {
            activity.supportActionBar!!.title = getText(R.string.restore)
        }
        viewModel = ViewModelProvider(this).get(RestoreViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        //TODO: get userid
        //val registrationNumber = "CL-1455"

        binding.searchButtonRef.setOnClickListener {
            if (viewModel.ref.value == null)
                   requireContext().toast(getString(R.string.enter_ref))
            else
                viewModel.getTransfertByref()
        }
        viewModel.ref.observe(viewLifecycleOwner){
            if (!viewModel.isNameValid(it) )
             binding.ref.error = getString(R.string.invalid_ref)

        }

        binding.aservirButton.setOnClickListener {
            viewModel.getTransfertsByEtat(registrationNumber)
        }
        viewModel.transfertCall.observe(viewLifecycleOwner) { response ->
            val responseTransferts = response.body()
            if (response.isSuccessful && responseTransferts != null) {
                viewModel.transfert.value = responseTransferts
                val format = SimpleDateFormat("dd/MM/yyy", Locale.ENGLISH)
                binding.datetransfert.text =
                    format.format(viewModel.transfert.value!!.dateTransfert!!)
                viewModel.getBeneficiaryClient(viewModel.transfert.value!!.idBeneficiaire)
                if (viewModel.transfert.value!!.etat!! == Etat.A_SERVIR )
                    binding.button.visibility = View.VISIBLE
                else
                    binding.button.visibility = View.GONE

            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        viewModel.beneficiaryClient.observe(viewLifecycleOwner) { response ->
            val benResponse = response.body()
            if (response.code() == 200 && benResponse != null) {
                viewModel.nomBeneficiary.value =
                    benResponse.lastName!!.uppercase() + " " + benResponse.firstName!!.replaceFirstChar { it.uppercase() }
                binding.item.visibility = View.VISIBLE
            } else {
                Log.i(
                    "request error",
                    "error code ${response.code()} message ${response.message()}"
                )
            }
        }
        binding.button.setOnClickListener {
            val bundle = bundleOf(
                "transfert" to viewModel.transfert.value,
                "nameBenficiaryCLient" to viewModel.nomBeneficiary.value
            )
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_restoreFragment_to_validateRestoreFragment, bundle)
        }
        //for the recycler
        var sizeIds = 0
        viewModel.listTransferts.observe(viewLifecycleOwner) { response ->
            val responseTransferts = response.body()

            if (response.isSuccessful && !responseTransferts.isNullOrEmpty()) {

                transferts = responseTransferts as ArrayList<Transfert>
                transferts = ArrayList(transferts.filter { transfert ->
                    transfert.dateExpiration!!.isAfter(LocalDateTime.now())
                }.sortedByDescending { transfert -> transfert.dateTransfert })
                val beneficiaryClientsIds =
                    responseTransferts.map { transfert -> transfert.idBeneficiaire }.distinct()
                sizeIds = beneficiaryClientsIds.size
                beneficiaryClientsIds.forEach { id ->
                    viewModel.apply {
                        getOneBeneficiaryClient(id!!)
                        oneBeneficiaryClient.observe(viewLifecycleOwner) { response ->
                            val benResponse = response.body()
                            if (response.code() == 200 && benResponse != null) {
                                listBens.value?.add(benResponse)
                                if (sizeIds == listBens.value?.size)
                                    this.lastIsReached.value = true
                            } else {
                                Log.i(
                                    "request error",
                                    "error code ${response.code()} message ${response.message()}"
                                )
                            }

                        }
                    }
                }
            }
        }
        viewModel.lastIsReached.observe(viewLifecycleOwner) {
            if (it) {
                beneficiaryClients = viewModel.listBens.value!!
                binding.recycler.layoutManager =
                    LinearLayoutManager(view?.context, RecyclerView.VERTICAL, false)
                binding.recycler.addItemDecoration(
                    DividerItemDecoration(
                        context,
                        DividerItemDecoration.VERTICAL
                    )
                )
                val adapter = RestoreAdapter(
                    transferts as ArrayList<Transfert>?,
                    beneficiaryClients
                )
                binding.recycler.adapter = adapter
                binding.recycler.visibility = View.VISIBLE
            }
        }
        return binding.root
    }


}