package com.example.transfertnational.network

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.transfertnational.utils.Constants
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*
import java.util.concurrent.TimeUnit


class NetworkingService {

    companion object {
        lateinit var apiRest: ApiRest
        lateinit var apiRestLogin: ApiRest


        @SuppressLint("SimpleDateFormat")
        @RequiresApi(Build.VERSION_CODES.O)
        fun init(context: Context) {

            //val json = GsonBuilder().setLenient().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()

            val json = GsonBuilder().registerTypeAdapter(
                LocalDateTime::class.java,
                JsonDeserializer<Any?> { json, _, _ ->
                    LocalDateTime.parse(json.asJsonPrimitive.asString)
                })
                .registerTypeAdapter(
                    Date::class.java,
                    JsonDeserializer<Date> { jsonElement: JsonElement, type: Type, jsonDeserializationContext: JsonDeserializationContext ->

                        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                        try {
                            return@JsonDeserializer df.parse(jsonElement.asString)
                        } catch (e: java.text.ParseException) {
                            e.printStackTrace();
                            return@JsonDeserializer null
                        }
                    })
                //.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()


            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(json))
                .client(okhttpClient(context))
                .build()

            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client: OkHttpClient = OkHttpClient
                .Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()
            val retrofitLogin: Retrofit = Retrofit.Builder()
                .baseUrl(Constants.AUTH_URL)
                .addConverterFactory(GsonConverterFactory.create(json))
                .client(client)
                .build()



            apiRest = retrofit.create(ApiRest::class.java)
            apiRestLogin = retrofitLogin.create(ApiRest::class.java)
        }

        private fun okhttpClient(context: Context): OkHttpClient {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            return OkHttpClient.Builder()
                .addInterceptor(AuthInterceptor(context))
                .addInterceptor(interceptor)
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100,TimeUnit.SECONDS)
                .build()
        }

    }

}