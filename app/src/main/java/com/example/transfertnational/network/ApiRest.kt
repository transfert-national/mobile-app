package com.example.transfertnational.network

import com.example.transfertnational.data.*
import com.example.transfertnational.data.enum.Etat
import com.example.transfertnational.data.model.*
import com.example.transfertnational.exceptions.BusinessException
import com.example.transfertnational.network.params.LoginInput
import com.example.transfertnational.network.params.WithdrawInput
import com.example.transfertnational.network.responses.AccessToken
import com.example.transfertnational.network.responses.LoginResponse
import com.example.transfertnational.utils.Constants.Companion.ACCOUNT_PATH
import com.example.transfertnational.utils.Constants.Companion.BENEFICIARY_CLIENT_PATH
import com.example.transfertnational.utils.Constants.Companion.CLIENT_PATH
import com.example.transfertnational.utils.Constants.Companion.OTP_PATH
import com.example.transfertnational.utils.Constants.Companion.TRANSFERT_PATH
import com.example.transfertnational.utils.Constants.Companion.USER_PATH
import com.example.transfertnational.utils.Constants.Companion.WALLET_PATH
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface ApiRest {

    @FormUrlEncoded
    @POST("/auth/realms/tarnsfert-national/protocol/openid-connect/token")
    suspend fun getAccessToken(
        @Field("client_id") client_id:String,
        @Field("grant_type") grant_type:String,
        @Field("client_secret") client_secret:String,
        @Field("scope") scope:String,
        @Field("username") username:String,
        @Field("password") password:String
    ):Response<AccessToken>

    @FormUrlEncoded
    @POST("/auth/realms/tarnsfert-national/protocol/openid-connect/logout")
    fun logout(
        @Field("client_id") client_id:String,
        @Field("refresh_token") refresh_token:String,
        @Field("client_secret") client_secret:String
    ):Call<ResponseBody>

    @Headers("Content-Type: application/json")
    @POST("$USER_PATH/login")
    suspend fun login(loginInput: LoginInput): Response<LoginResponse>

    @GET("$CLIENT_PATH/{registrationNumber}")
    suspend fun getClientByRegistrationNumber(@Path("registrationNumber") registrationNumber: String?): Response<Client>

    @GET("$WALLET_PATH/client/{registrationNumber}")
    suspend fun getWalletByClient(@Path("registrationNumber") registrationNumber: String?): Response<Wallet>

    @GET("$ACCOUNT_PATH/client/{clientId}")
    suspend fun getAccountByClientId(@Path("clientId") clientId: String?): Response<Account>

    @GET("$TRANSFERT_PATH/user/{idUser}")
    suspend fun getTransfertsByUser(@Path("idUser") idUser: String?): Response<List<Transfert>>

    @GET("$BENEFICIARY_CLIENT_PATH/client/{id}")
    suspend fun getBeneficiaryClientByClient(@Path("id") idClient: String?): Response<List<BeneficiaryClient>>

    @GET("$BENEFICIARY_CLIENT_PATH/{id}")
    suspend fun getBeneficiaryClient(@Path("id") id: String?): Response<BeneficiaryClient>

    @POST(BENEFICIARY_CLIENT_PATH)
    suspend fun addBeneficiaryClient(@Body beneficiaryClient : BeneficiaryClient ):Response<BeneficiaryClient>

    @POST("$OTP_PATH/generate")
    fun generateClientOtp():Call<Void>


    @POST("$CLIENT_PATH/otp/verify")
    suspend fun verifyOtp(@Body otp: Otp?): Boolean?

    @POST("$CLIENT_PATH/transfer/debit")
    suspend fun makeTransferFromCompteDebit(@Body transfert: Transfert?): Response<Transfert>

    @PUT("$WALLET_PATH/client/deposit")
    suspend fun depositByClientId(
        @Query("registrationNumber") registrationNumber: String?,
        @Query("amount") amount: Double
    ): Response<Wallet>

    @PUT("$ACCOUNT_PATH/withdraw/client/")
    suspend fun withdrawByRegistrationNumber(@Body withdrawDto: WithdrawInput): Response<Account>

    @GET("$TRANSFERT_PATH/{ref}")
    suspend fun getTransfertByRef(@Path("ref") ref: String?): Response<Transfert>

    @GET("$TRANSFERT_PATH/state/{etat}/user/{idUser}")
    suspend fun getTransfertByEtatAndUser(
        @Path("etat") etat: Etat?,
        @Path("idUser") idUser: String?
    ): Response<List<Transfert>>


    @PUT("$CLIENT_PATH/transfer/restore")
    suspend fun changeTransferEtat(
        @Query("ref") ref: String?,
        @Query("motif") motif: String?
    ): Response<Transfert>
}