package com.example.transfertnational.network.params

data class WithdrawInput(
    var registrationNumber:String,
    var amount:Double
)
