package com.example.transfertnational.network.responses

import com.example.transfertnational.data.model.User

data class LoginResponse (
    val user: User?,
    val accessToken: String?,
    val roles: ArrayList<String>? ){
}