package com.example.transfertnational.data.enum

enum class TypeFrais {
    FRAIS_A_LA_CHARGE_DONNEUR, FRAIS_A_LA_CHARGE_BENEFICIARE, FRAIS_PARTAGES
}
