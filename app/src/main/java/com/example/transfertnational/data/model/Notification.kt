package com.example.transfertnational.data.model

data class Notification (
    var message: String?,
    var subject: String?,
    var emailDestinataire: String?
)