package com.example.transfertnational.data.model

data class Wallet (
    var walletNumber: String?,
    var balance: Double?,
    var registrationNumber: String?
)