package com.example.transfertnational.data.model

import java.util.*

data class Otp(var codePin:Int = 0 ,
               var dateExpiration: Date? = null,
               var clientId: String? = null,
               var idTransfert: String? = null)
