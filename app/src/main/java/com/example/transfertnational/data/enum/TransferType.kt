package com.example.transfertnational.data.enum

enum class TransferType {
    BY_ACCOUNT_DEBIT, BY_WALLET_DEBIT, IN_CASH
}

