package com.example.transfertnational.data.model


data class Account (
    var id: Long?,
    var registrationNumber: String?,
    var accountType: String?,
    var balance :Double,
    var rib: String?
)
