package com.example.transfertnational.data.model

data class BeneficiaryClient (
        var id: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var phoneNumber: String? = null,
        var email: String? = null,
        var clientId:String? = null
){
        override fun toString(): String {
                return "${lastName!!.uppercase()} ${firstName!!.replaceFirstChar { it.uppercase() }}, $phoneNumber"
        }

}