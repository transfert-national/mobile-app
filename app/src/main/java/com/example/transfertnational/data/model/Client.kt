package com.example.transfertnational.data.model

import java.time.LocalDateTime
import java.util.*

data class Client (
    var cin: String?,
    var registrationNumber: String?,
    var firstName: String?,
    var lastName: String?,
    var dateOfBirth: Date?,
    var profession: String?,
    var city: String?,
    var address: String?,
    var gsm: String?,
    var email: String?
)
