package com.example.transfertnational.data.model


data class User (
    var id: Long?,
    var username: String?,
    var password: String?,
    var registrationNumber: String?,
    var roles: Collection<Role> = ArrayList()
){

}