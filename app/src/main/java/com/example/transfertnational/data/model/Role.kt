package com.example.transfertnational.data.model

import com.example.transfertnational.data.enum.RoleType

data class Role(
    var id:Long,
    var roleName: RoleType
)
