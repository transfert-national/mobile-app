package com.example.transfertnational.data.enum

enum class Etat(val str: String) {
    A_SERVIR("A servir"), SERVIE("servie"), EXTOURNE("extourné"), RESTITUE("restitué"), BLOQUE("bloqué"), DEBLOQUE(
        "debloqué"
    )
}
