package com.example.transfertnational.data.model

import android.os.Parcelable
import com.example.transfertnational.data.enum.Etat
import com.example.transfertnational.data.enum.TransferType
import com.example.transfertnational.data.enum.TypeFrais
import kotlinx.parcelize.Parcelize
import java.time.LocalDateTime
import java.util.*

@Parcelize
data class Transfert (
    var ref: String? = null,
    var montant: Long? = null,
    var total: Double? = null,
    var idUser: String? = null,
    var idDonneur: String? = null,
    var idBeneficiaire: String? = null,
    var etat: Etat? = null,
    var typeFrais: TypeFrais? = null,
    var motif: String? = null,
    var dateExpiration: LocalDateTime? = null,
    var transferNotification:Boolean = false,
    var transferType: TransferType? = null,
    var dateTransfert: Date? = null,
    var dateServir:Date? = null
) : Parcelable{

}
