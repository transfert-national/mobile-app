package com.example.transfertnational.utils

class Constants {
    companion object{
        const val BASE_URL = "http://46.101.113.96:8080//"
        const val AUTH_URL = "http://keycloak.ensas-bank.tk:8180/"
        const val CLIENT_PATH ="/clients"
        const val BENEFICIARY_CLIENT_PATH ="/beneficiary_clients"
        const val WALLET_PATH ="/wallets"
        const val ACCOUNT_PATH ="/accounts"
        const val OTP_PATH ="/otps/client"
        const val USER_PATH ="/users"
        const val TRANSFERT_PATH ="/transferts"
        const val CLIENT_ID = "transfer-national-mobile"
        const val GRANT_TYPE = "password"
        const val CLIENT_SECRET = "5a0d813a-440d-4fa3-8c17-cdb5cab2e1f2"
        const val SCOPE = "openid"
        const val USER_TOKEN = "user_token"
        const val REFRESH_TOKEN = "refresh_token"
        var registrationNumber = ""

    }

}