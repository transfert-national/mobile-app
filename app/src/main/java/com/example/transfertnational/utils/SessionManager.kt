package com.example.transfertnational.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.transfertnational.R
import com.example.transfertnational.utils.Constants.Companion.REFRESH_TOKEN
import com.example.transfertnational.utils.Constants.Companion.USER_TOKEN

class SessionManager(context: Context) {
    private var prefs: SharedPreferences =
        context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE)


    /**
     * Function to save auth token
     */
    fun saveAuthToken(token: String?) {
        val editor = prefs.edit()
        editor.putString(USER_TOKEN, token)
        editor.apply()
    }
    fun saveRefreshToken(refresh_token: String?) {
        val editor = prefs.edit()
        editor.putString(REFRESH_TOKEN, refresh_token)
        editor.apply()
    }

    /**
     * Function to fetch auth token
     */
    fun fetchAuthToken(): String {
        return prefs.getString(USER_TOKEN, null).orEmpty()
    }
    fun fetchRefreshToken(): String {
        return prefs.getString(REFRESH_TOKEN, null).orEmpty()
    }
}